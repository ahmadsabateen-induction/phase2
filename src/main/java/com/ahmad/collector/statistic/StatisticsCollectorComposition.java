package com.ahmad.collector.statistic;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
public class StatisticsCollectorComposition<T,S extends Statistic> implements StatisticsCollector<T,S> {
    private final List<StatisticsCollector<T,S>> calculators;
    private  final String name;
    public StatisticsCollectorComposition(String name, List<StatisticsCollector<T,S>> calculators) {
        this.calculators = calculators;
        this.name = name;
    }
    @Override
    public String getName() {
        return name;
    }
    public Iterable<S> collectStatistics(Iterable<T> data) {
        List<S> statistics = new ArrayList<>();
        for (StatisticsCollector<T,S> calculator : calculators) {
            statistics.addAll((Collection<? extends S>) calculator.collectStatistics(data));
        }
        return statistics;
    }
}