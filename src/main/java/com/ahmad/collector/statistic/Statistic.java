package com.ahmad.collector.statistic;

public interface Statistic {
     String getKey();
     Object getValue();

}

