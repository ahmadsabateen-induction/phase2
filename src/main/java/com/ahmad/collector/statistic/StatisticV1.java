package com.ahmad.collector.statistic;

import java.util.Objects;

public class StatisticV1 implements Statistic{
    private final String key;
    private final Object value;
    public StatisticV1(String key, Object value) {
        this.key = key;
        this.value = value;
    }
    @Override
    public String getKey() {
        return key;
    }
    @Override
    public Object getValue() {
        return value;
    }
    @Override
    public boolean equals(Object other) {
        if (this == other) return true;
        if (other == null || getClass() != other.getClass()) return false;
        StatisticV1 otherStatistics = (StatisticV1) other;
        return Objects.equals(key, otherStatistics.key) &&
                Objects.equals(value, otherStatistics.value);
    }
    @Override
    public int hashCode() {
        return Objects.hash(key, value);
    }

}
