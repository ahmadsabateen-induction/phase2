package com.ahmad.collector.calculators.employee;

import com.ahmad.collector.beans.Employee;

public class ResignationYearStatisticsCalculator extends AbstractStatisticsCalculator {
    public static final String RESIGNATION_YEAR_KEY = "Employees by resignation year ";
    @Override
    protected Object getKey(Employee employee) {
        return RESIGNATION_YEAR_KEY+employee.getResignationDate().getYear();
    }
}
