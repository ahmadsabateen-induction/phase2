package com.ahmad.collector.calculators.employee;

import com.ahmad.collector.beans.Employee;

public class BirthYearStatisticsCalculator extends AbstractStatisticsCalculator {
    private static final String BIRTH_YEAR_KEY = "Employees by birth year ";
    @Override
    protected Object getKey(Employee employee) {
        return BIRTH_YEAR_KEY+employee.getBirthDate().getYear();
    }
}