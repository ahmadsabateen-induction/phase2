package com.ahmad.collector.calculators.employee;

import com.ahmad.collector.beans.Employee;

public class SalaryRangeStatisticsCalculator extends AbstractStatisticsCalculator {
    private static final String SALARY_MORE_THAN_1200 = "Salary > 1200";
    private static final String SALARY_LESS_THAN_350 = "Salary <= 350";
    private static final String SALARY_MORE_THAN_350_AND_LESS_THAN_600 = "350 < Salary <= 600";
    private static final String SALARY_MORE_THAN_600_AND_LESS_THAN_1200 = "600 < Salary <= 1200";
    @Override
    protected Object getKey(Employee employee) {
        return calculateSalaryRange(employee.getSalary());
    }

    private String calculateSalaryRange(double salary) {
        if (salary <= 350) return SALARY_LESS_THAN_350;
        if (salary > 350 && salary <= 600) return SALARY_MORE_THAN_350_AND_LESS_THAN_600;
        if (salary > 600 && salary <= 1200) return SALARY_MORE_THAN_600_AND_LESS_THAN_1200;
        return SALARY_MORE_THAN_1200;
    }
}
