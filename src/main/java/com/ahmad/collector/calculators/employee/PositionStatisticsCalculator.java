package com.ahmad.collector.calculators.employee;

import com.ahmad.collector.beans.Employee;

public class PositionStatisticsCalculator extends AbstractStatisticsCalculator {
    private static final String POSITION_KEY = "Employees by position ";
    @Override
    protected Object getKey(Employee employee) {
        return POSITION_KEY+employee.getPosition();
    }
}