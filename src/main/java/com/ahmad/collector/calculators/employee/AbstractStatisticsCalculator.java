package com.ahmad.collector.calculators.employee;

import com.ahmad.collector.beans.Employee;
import com.ahmad.collector.statistic.Statistic;
import com.ahmad.collector.statistic.StatisticV1;
import com.ahmad.collector.statistic.StatisticsCollector;

import java.util.*;

public abstract class AbstractStatisticsCalculator implements StatisticsCollector<Employee, Statistic> {
    private static final String STATISTIC_NAME = "Employee Statistic";
    @Override
    public String getName() {
        return STATISTIC_NAME;
    }

    @Override
    public Iterable<Statistic> collectStatistics(Iterable<Employee> employees) {
        List<Statistic> statistics = new ArrayList<>();
        Map<Object, Integer> countMap = new HashMap<>();
        for (Employee employee : employees) {
            Object key = getKey(employee);
            countMap.put(key, countMap.getOrDefault(key, 0) + 1);
        }
        for (Map.Entry<Object, Integer> entry : countMap.entrySet()) {
            statistics.add(new StatisticV1((String) entry.getKey(), entry.getValue()));
        }
        return statistics;
    }

    protected abstract Object getKey(Employee employee);
}