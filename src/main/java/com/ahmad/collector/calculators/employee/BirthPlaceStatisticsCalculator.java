package com.ahmad.collector.calculators.employee;

import com.ahmad.collector.beans.Employee;

public class BirthPlaceStatisticsCalculator extends AbstractStatisticsCalculator {
    private static final String BIRTH_PLACE_KEY = "Employees by birth place ";
    @Override
    protected Object getKey(Employee employee) {
        return BIRTH_PLACE_KEY+employee.getBirthPlace();
    }
}
