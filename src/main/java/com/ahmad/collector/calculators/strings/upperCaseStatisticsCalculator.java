package com.ahmad.collector.calculators.strings;

public class upperCaseStatisticsCalculator extends CharacterStatisticsCalculator {
    private final  static String UPPER_CASE_STATISTICS_KEY = "Upper case characters";

    public upperCaseStatisticsCalculator() {
        statisticKey = UPPER_CASE_STATISTICS_KEY;
    }
    @Override
    protected boolean isValidChar(char character) {
        return Character.isUpperCase(character);
    }
}
