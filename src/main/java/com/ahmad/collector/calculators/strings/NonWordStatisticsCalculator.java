package com.ahmad.collector.calculators.strings;

public class NonWordStatisticsCalculator extends CharacterStatisticsCalculator {
    private final static String NON_WORD_STATISTICS_KEY = "Non-word characters";

    public NonWordStatisticsCalculator() {
        statisticKey = NON_WORD_STATISTICS_KEY;
    }
    @Override
    protected boolean isValidChar(char character) {
        return !Character.isLetterOrDigit(character);
    }
}
