package com.ahmad.collector.calculators.strings;

import com.ahmad.collector.statistic.Statistic;
import com.ahmad.collector.statistic.StatisticV1;
import com.ahmad.collector.statistic.StatisticsCollector;

import java.util.Collections;

public abstract class CharacterStatisticsCalculator implements StatisticsCollector<String, Statistic> {
    private static final String STATISTICS_NAME = "Character Statistic";
    protected String statisticKey;
    @Override
    public String getName() {
        return STATISTICS_NAME;
    }

    @Override
    public Iterable<Statistic> collectStatistics(Iterable<String> data) {
        int charCount = 0;
        for (String string : data) {
            for (int currentChar = 0; currentChar < string.length(); currentChar++) {
                char c = string.charAt(currentChar);
                if (isValidChar(c)) {
                    charCount++;
                }
            }
        }
        return Collections.singletonList(new StatisticV1(statisticKey,charCount));
    }
    protected abstract boolean isValidChar(char character);
}
