package com.ahmad.collector.calculators.strings;

import com.ahmad.collector.statistic.StatisticV1;
import com.ahmad.collector.statistic.Statistic;
import com.ahmad.collector.statistic.StatisticsCollector;

import java.util.Collections;

public class WordCountStatisticsCalculator implements StatisticsCollector<String, Statistic> {
    private final static String WORDS_COUNT_STATISTICS_KEY = "Words";

    @Override
    public String getName() {
        return WORDS_COUNT_STATISTICS_KEY;
    }

    @Override
    public Iterable<Statistic> collectStatistics(Iterable<String> data) {
        int wordCount = 0;
        for (String string : data) {
            String[] words = string.split("\\s+");
            wordCount += words.length;
        }
        return Collections.singletonList(new StatisticV1(WORDS_COUNT_STATISTICS_KEY,wordCount));
    }
}
