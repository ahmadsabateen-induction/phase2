package com.ahmad.collector.calculators.strings;

public class NumbersStatisticsCalculator extends CharacterStatisticsCalculator {
    private final static String NUMBER_STATISTICS_KEY = "Numbers";
    public NumbersStatisticsCalculator() {
        statisticKey = NUMBER_STATISTICS_KEY;
    }
    @Override
    protected boolean isValidChar(char character) {
        return Character.isDigit(character);
    }
}
