package com.ahmad.collector.calculators.strings;

public class LowerCaseStatisticsCalculator extends CharacterStatisticsCalculator {
    private final static String LOWER_CASE_STATISTICS_KEY = "Lower case characters";

    public LowerCaseStatisticsCalculator() {
        statisticKey = LOWER_CASE_STATISTICS_KEY;
    }
    @Override
    protected boolean isValidChar(char character) {
        return Character.isLowerCase(character);
    }

}
