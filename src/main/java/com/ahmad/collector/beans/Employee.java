package com.ahmad.collector.beans;

import lombok.Data;

import java.time.LocalDate;

@Data
public class Employee {
    private String firstName;
    private String lastName;
    private LocalDate birthDate;
    private String birthPlace;
    private LocalDate hiringDate;
    private LocalDate resignationDate;
    private String position;
    private double salary;

    public Employee(String firstName, String lastName, LocalDate birthDate, String birthPlace, LocalDate hiringDate, LocalDate resignationDate, String position, double salary) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.birthPlace = birthPlace;
        this.hiringDate = hiringDate;
        this.resignationDate = resignationDate;
        this.position = position;
        this.salary = salary;
    }
}
