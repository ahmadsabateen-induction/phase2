import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.ahmad.collector.calculators.strings.*;
import com.ahmad.collector.statistic.Statistic;
import com.ahmad.collector.statistic.StatisticV1;
import com.ahmad.collector.statistic.StatisticsCollector;
import com.ahmad.collector.statistic.StatisticsCollectorComposition;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class StringStatisticsCollectorTest {
    @Test
    public void testCollectStatistics() {
        List<StatisticsCollector<String, Statistic>> statisticsCalculators = new ArrayList<>();

        statisticsCalculators.add(new upperCaseStatisticsCalculator());
        statisticsCalculators.add(new LowerCaseStatisticsCalculator());
        statisticsCalculators.add(new NonWordStatisticsCalculator());
        statisticsCalculators.add(new WordCountStatisticsCalculator());
        statisticsCalculators.add(new NumbersStatisticsCalculator());

        StatisticsCollector<String, Statistic> collector = new StatisticsCollectorComposition<>("String Statistics",statisticsCalculators);

        List<String> strings = Arrays.asList("Hello World!", "Test123", "AhmadSabateen");

        Iterable<Statistic> statistics = collector.collectStatistics(strings);

        List<Statistic> expectedStatistics = new ArrayList<>();
        expectedStatistics.add(new StatisticV1("Upper case characters",5));
        expectedStatistics.add(new StatisticV1("Lower case characters",22));
        expectedStatistics.add(new StatisticV1("Non-word characters",2));
        expectedStatistics.add(new StatisticV1("Words",4));
        expectedStatistics.add(new StatisticV1("Numbers",3));
        assertIterableEquals(expectedStatistics, statistics);
    }
}
// TODO
// add new emp
// choose employee stats
// map/jdbc/file
// enter string/ stats
// stream
// given/when/then
// class names should be consistence
// protected String statisticKey; should be private and passed by constructor
