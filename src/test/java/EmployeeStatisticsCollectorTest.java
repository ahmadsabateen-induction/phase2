import com.ahmad.collector.beans.Employee;
import com.ahmad.collector.calculators.employee.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.ahmad.collector.statistic.StatisticsCollectorComposition;
import com.ahmad.collector.statistic.Statistic;
import com.ahmad.collector.statistic.StatisticsCollector;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class EmployeeStatisticsCollectorTest {
    StatisticsCollector<Employee, Statistic> collector;
    @BeforeEach
    public void setUp(){
        List<StatisticsCollector<Employee,Statistic>> statisticsCalculators = new ArrayList<>();
        statisticsCalculators.add(new BirthYearStatisticsCalculator());
        statisticsCalculators.add(new BirthPlaceStatisticsCalculator());
        statisticsCalculators.add(new SalaryRangeStatisticsCalculator());
        statisticsCalculators.add(new PositionStatisticsCalculator());
        statisticsCalculators.add(new ResignationYearStatisticsCalculator());
         collector = new StatisticsCollectorComposition<>("Employee Statistics",statisticsCalculators);
    }
    @Test
    public void testCollectStatistics() {
        Employee e1 = new Employee("Ahmad", "Sabateen", LocalDate.of(1990, 1, 1), "USA", LocalDate.of(2010, 1, 1), LocalDate.of(2018, 12, 31), "Manager", 600);
        Employee e2 = new Employee("Ahmad", "Darabkah", LocalDate.of(1995, 5, 5), "UK", LocalDate.of(2015, 1, 1), LocalDate.of(2020, 12, 31), "Developer", 800);
        Employee e3 = new Employee("Ali", "Something", LocalDate.of(2000, 10, 10), "USA", LocalDate.of(2017, 1, 1), LocalDate.of(2020, 12, 31), "Developer", 400);
        Employee e4 = new Employee("Mohammad", "SomethingElse", LocalDate.of(1985, 3, 3), "UK", LocalDate.of(2010, 1, 1), LocalDate.of(2015, 12, 31), "Manager", 1200);
        List<Employee> employees = Arrays.asList(e1, e2, e3, e4);

        Iterable<Statistic> statistics = collector.collectStatistics(employees);

        assertEquals(1, getCount(statistics,"Employees by birth year 1990"));
        assertEquals(1, getCount(statistics,"Employees by birth year 1995"));
        assertEquals(1, getCount(statistics,"Employees by birth year 2000"));
        assertEquals(1, getCount(statistics,"Employees by birth year 1985"));

        assertEquals(2, getCount(statistics,"Employees by birth place UK"));
        assertEquals(2, getCount(statistics,"Employees by birth place USA"));

        assertEquals(0, getCount(statistics,"Salary <= 350"));
        assertEquals(2, getCount(statistics,"350 < Salary <= 600"));
        assertEquals(2, getCount(statistics,"600 < Salary <= 1200"));
        assertEquals(0, getCount(statistics,"Salary > 1200"));

        assertEquals(1, getCount(statistics,"Employees by resignation year 2018"));
        assertEquals(2, getCount(statistics,"Employees by resignation year 2020"));
        assertEquals(1, getCount(statistics,"Employees by resignation year 2015"));

        assertEquals(2, getCount(statistics,"Employees by position Manager"));
        assertEquals(2, getCount(statistics,"Employees by position Developer"));
    }
    private int getCount(Iterable<Statistic> statistics, String key) {
        for (Statistic statistic: statistics) {
            if (Objects.equals(statistic.getKey(), key)){
                return (int) statistic.getValue();
            }
        }
        return 0;
    }
}
